<?php
    /**
     * Plugin Name: Quimo Compound
     * Plugin URI:
     * Description: Simple WordPress template chunks manager
     * Version: 1.0
     * Author: Simone Alati
     * Author URI: http://www.simonealati.it
     */

     /*
     ██████╗ ██████╗ ███╗   ███╗██████╗  ██████╗ ██╗   ██╗███╗   ██╗██████╗
     ██╔════╝██╔═══██╗████╗ ████║██╔══██╗██╔═══██╗██║   ██║████╗  ██║██╔══██╗
     ██║     ██║   ██║██╔████╔██║██████╔╝██║   ██║██║   ██║██╔██╗ ██║██║  ██║
     ██║     ██║   ██║██║╚██╔╝██║██╔═══╝ ██║   ██║██║   ██║██║╚██╗██║██║  ██║
     ╚██████╗╚██████╔╝██║ ╚═╝ ██║██║     ╚██████╔╝╚██████╔╝██║ ╚████║██████╔╝
     ╚═════╝ ╚═════╝ ╚═╝     ╚═╝╚═╝      ╚═════╝  ╚═════╝ ╚═╝  ╚═══╝╚═════╝

     ID
     post_author
     post_date
     post_date_gmt
     post_content
     post_title
     post_excerpt
     post_status
     comment_status
     ping_status
     post_password
     post_name
     to_ping
     pinged
     post_modified
     post_modified_gmt
     post_content_filtered
     post_parent
     guid
     menu_order
     post_type
     post_mime_type
     comment_count
     filter
     post_thumbnail_full
     */

    class Compound {

        public static function render() {

            global $post;

            $args = array(
            	'sort_order' => 'asc',
            	'sort_column' => 'menu_order',
            	'hierarchical' => 0,
            	'parent' => $post->ID,
            	'post_type' => 'page',
            	'post_status' => 'private'
            );
            $pages = get_pages($args);
            for ($i = 0; $i < count($pages); $i++) {
                //aggiungo l'immagine in evidenza
                $pages[$i]->post_thumbnail_full = self::addFeaturedImage($pages[$i]->ID);
                //recupero il nome del template
                $template_name = get_page_template_slug($pages[$i]->ID);
                if ($template_name) {
                    $template = file_get_contents(get_stylesheet_directory_uri().'/'.$template_name);

                    /* se ho installato il plugin ACF */
                    if (function_exists('get_fields')) {
                        $fields = get_fields($pages[$i]->ID);
                        foreach ($fields as $name => $value) {
                            if (is_array($value) && !empty($value)) {
                                /* se il campo è un repeater verifico che ci sia il segnaposto del repeater */
                                $pos = strpos($template, '[+'.$name.'+]');
                                if ($pos !== false) {
                                    /* recupero la sezione di template da ripetere */
                                    $section = self::getStringBetween($template, '[+'.$name.'+]', '[+/'.$name.'+]');
                                    $template = str_replace('[+'.$name.'+]'.$section.'[+/'.$name.'+]', "", $template);
                                    $html = '';
                                    for ($j = 0; $j < count($value); $j++) {
                                        /* ciclo nei sottocampi del repeater */
                                        $sub_template = $section;
                                        foreach ($value[$j] as $sub_name => $sub_value) {
                                            $sub_template = str_replace("[+".$sub_name."+]",$sub_value,$sub_template);
                                        }
                                        $html .= $sub_template;
                                    }
                                    $template = substr_replace($template, $html, $pos, 0);
                                }
                            } else {
                                /* se il campo non è un repeater */
                                if ($value) $template = str_replace("[+".$name."+]",$value,$template);
                            }
                        }
                    }

                    //percorro i campi estratti da get_pages
                    foreach($pages[$i] as $key => $value) {
                        $template = str_replace("[+".$key."+]",$value,$template);
                    }
                    echo $template;
                }
            }
        }

        public static function renderPart($slug) {
            $args = array(
            	'name' => $slug,
            	'post_type' => 'page',
            	'post_status' => 'private'
            );
            $page = get_posts($args);
            $page[0]->post_thumbnail_full = self::addFeaturedImage($page[0]->ID);
            $template_name = get_page_template_slug($page[0]->ID);
            if ($template_name) {
                $template = file_get_contents(get_stylesheet_directory_uri().'/'.$template_name);
                //percorro i campi estratti da get_pages
                foreach($page[0] as $key => $value) {
                    $template = str_replace("[+".$key."+]",$value,$template);
                }
                echo $template;
            }
        }

        private static function addFeaturedImage($id) {
            if (function_exists('has_post_thumbnail') && has_post_thumbnail($id)) {
                $dummy = wp_get_attachment_image_src(get_post_thumbnail_id($id),'full');
                return $dummy[0];
            }
            return 0;
        }

        private static function getStringBetween($string, $start, $end) {
            $string = ' ' . $string;
            $ini = strpos($string, $start);
            if ($ini == 0) return '';
            $ini += strlen($start);
            $len = strpos($string, $end, $ini) - $ini;
            return substr($string, $ini, $len);
        }

    }
?>
