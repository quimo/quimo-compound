<!DOCTYPE HTML>
<html>
    <head>
		<title></title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<?php wp_head() ?>
    </head>
    <body>
		<main>
			<?php 
			/* recupera la pagina con slug 'sezione1' */
			Compound::renderPart('sezione1');
			Compound::renderPart('sezione2');
			?>
		</main>
		<aside>
			<?php 
			Compound::renderPart('barra-laterale');
			?>
		</aside>
		<footer>
			<?php wp_footer() ?>
        </footer>
    </body>
</html>