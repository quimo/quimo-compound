<?php
    /* Template Name: sezione1 */
?>
<div class="servizio">
    <div class="servizio__descrizione">
        <header>
            <h3>[+post_title+]</h3>
        </header>
        [+post_content+]
    </div>
    <div class="servizio__immagine" style="background: url('[+post_thumbnail_full+]') top left no-repeat; background-size: cover;"></div>
</div>