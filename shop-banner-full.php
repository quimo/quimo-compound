<?php
/**
 * Template Name: Shop Banner 100%
 */
?>
<div class="shop-grid">
    <div class="shop-cell-100">
        <a class="desktop" href="[+shopgrid_link_100+]"><img src="[+shopgrid_banner_100+]"></a>
        <a class="mobile" href="[+shopgrid_link_100+]"><img src="[+shopgrid_banner_100_mobile+]"></a>
    </div>
</div>
