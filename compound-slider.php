<?php /* Template Name: Slider */ ?>
<section class="compound compound--slider">
    <h2 class="section-title">[+post_title+]</h2>
    <div class="owl-carousel owl-theme">
		<!-- compound_carousel_item è un campo ACF repeater -->
		<!-- all'interno di qeusta sezione (che viene ripetura tante volte quante sono le righe del repeater) ci sono i sub_field di ACF -->
        [+compound_carousel_item+]
        <div>
            <div class="compound__image compound--home" style="background: url([+compound_carousel_image+]) no-repeat center center; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
                <a href="[+compound_carousel_link+]">&nbsp;</a>
            </div>
        </div>
        [+/compound_carousel_item+]
		<!-- qua finisce la sezione del repeater -->
    </div>
</section>
