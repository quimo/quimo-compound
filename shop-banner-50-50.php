<?php
/**
 * Template Name: Shop Banner 50% 50%
 */
?>
<div class="shop-grid">
    <div class="shop-cell-50">
        <a href="[+shopgrid_link1_50+]"><img src="[+shopgrid_banner1_50+]"></a>
    </div>
    <div class="shop-cell-50">
        <a href="[+shopgrid_link2_50+]"><img src="[+shopgrid_banner2_50+]"></a>
    </div>
</div>
