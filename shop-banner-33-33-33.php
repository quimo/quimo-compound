<?php
/**
 * Template Name: Shop Banner 33% 33% 33%
 */
?>
<div class="shop-grid">
    <div class="shop-cell-33">
        <a href="[+shopgrid_link1_33+]"><img src="[+shopgrid_banner1_33+]"></a>
    </div>
    <div class="shop-cell-33">
        <a href="[+shopgrid_link2_33+]"><img src="[+shopgrid_banner2_33+]"></a>
    </div>
    <div class="shop-cell-33">
        <a href="[+shopgrid_link3_33+]"><img src="[+shopgrid_banner3_33+]"></a>
    </div>
</div>
