<?php
/**
 * Template Name: Shop Banner 33% 66%
 */
?>
<div class="shop-grid">
    <div class="shop-cell-33">
        <a href="[+shopgrid_link1_33_66+]"><img src="[+shopgrid_banner1_33_66+]"></a>
    </div>
    <div class="shop-cell-66">
        <a href="[+shopgrid_link2_33_66+]"><img src="[+shopgrid_banner2_33_66+]"></a>
    </div>
</div>
