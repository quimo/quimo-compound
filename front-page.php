<!DOCTYPE HTML>
<html>
    <head>
		<title></title>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<?php wp_head() ?>
    </head>
    <body>
		<main>
			<?php 
			/* recupera tutte le pagine figlie della pagina corrente in ordine menu_order */
			Compound::render();
			?>
		</main>
		<footer>
			<?php wp_footer() ?>
        </footer>
    </body>
</html>