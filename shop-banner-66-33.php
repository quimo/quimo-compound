<?php
/**
 * Template Name: Shop Banner 66% 33%
 */
?>
<div class="shop-grid">
    <div class="shop-cell-66">
        <a href="[+shopgrid_link1_66_33+]"><img src="[+shopgrid_banner1_66_33+]"></a>
    </div>
    <div class="shop-cell-33">
        <a href="[+shopgrid_link2_66_33+]"><img src="[+shopgrid_banner2_66_33+]"></a>
    </div>
</div>
