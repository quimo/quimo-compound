<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package themealley_business
 * Template Name: Shop filter
 */

?>

<?php get_header(); ?>

	<div id="primary">
		<div id="content" class="site-content" role="main">

		<?php Compound::render() ?>

		</div><!-- #content -->
	</div><!-- #primary -->
